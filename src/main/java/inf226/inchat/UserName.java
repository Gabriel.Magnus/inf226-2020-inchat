package inf226.inchat;

public final class UserName {
    public final String value;
    public UserName(String aValue) {
        value = aValue;
    }
    @Override
    public String toString() {
        return value;
    }
}
