package inf226.inchat;

public final class Password {
    public final String hashedValue;

    /***
     * Standard constructor for a password with a hashed value
     * @param aValue - The cleartext password
     * @param aHashedValue - The hash of the password
     * @throws IllegalArgumentException - if cleartext password does not satisfy NIST criteria
     */
    public Password(String aValue, String aHashedValue) throws IllegalArgumentException {
        if (!checkPasswordQuality(aValue))
            throw new IllegalArgumentException("Password is too weak");
        else {
            hashedValue = aHashedValue;
        }
    }

    /**
     * Constructor for use with hashed password only, e.g. when loading from a database
     * @param aHashedValue - the hash of the password
     */
    public Password(String aHashedValue) {
        hashedValue = aHashedValue;
    }

    private boolean checkPasswordQuality(String aValue) {
        return (aValue != null && aValue.length() >= 8 && aValue.length() <= 64);
    }

}
